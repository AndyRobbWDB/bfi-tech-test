import React , { useEffect , useState} from 'react';

import Card from 'react-bootstrap/Card'
import CardGroup from 'react-bootstrap/CardGroup'

import Select from 'react-select'

import { apiGet } from '../api/APIService'
import ArticlePagination from './ArticlePagination'

import './BFIArticlesList.css';

function BFIArticlesList() {

  const [loading, setLoading] = useState(true);
  const [articles, setArticles] = useState([]);
  const [articleCount, setArticleCount] = useState(0);
  const [types, setTypes] = useState([]);
  const [authors, setAuthors] = useState([]);
  const [selectedAuthor, setSelectedAuthor] = useState('');
  const [selectedType, setSelectedType] = useState('');
  const [page, setPage] = useState(1);
  const [colCount, setColCount] = useState(3);
  const [rowCount, setRowCount] = useState(3);
  const [pagesTotal, setpagesTotal] = useState(0);
  const [pageSize, setPageSize] = useState(rowCount * colCount);

  const pageSizeEventHandler  = (page_size) => {
    setPageSize(page_size);
    setPage(1);
  }

  const paginationEventHandler  = (page) => {
    setPage(parseInt(page, 10));
  }

  // runs once to populate Author and Type filters
  useEffect(() => {
    Promise.all([
        apiGet('https://content-store.explore.bfi.digital/api/types'),
        apiGet('https://content-store.explore.bfi.digital/api/authors')
   ])
   .catch(function(err) {
     //**TODO**//
   })
   .then(([retTypes, retAuthors]) => {
     if (retTypes)
      setTypes(retTypes.data);
     if (retAuthors)
      setAuthors(retAuthors.data);
    })
  },[])

  // runs each time filters or pagination changes
  useEffect(() => {

    setLoading(true);
    const retrieveData  = async () => {
        return apiGet(`https://content-store.explore.bfi.digital/api/articles?page=${page}&size=${pageSize}${'' !== selectedType ? `&type=${selectedType}` : ''}${'' !== selectedAuthor ? `&author=${selectedAuthor}` : ''}`)
    }

    retrieveData()
    .then(
        (result) => {
          setArticles(result.data);
          setpagesTotal(result.meta.last_page);
          setArticleCount(result.meta.total)
          setLoading(false);
        },
        (error) => {
          //**TODO**//

        }
    ).catch((err) => {
      //**TODO**//
    });

  }, [selectedAuthor, selectedType, page, pageSize]);

  const rows = [...Array( Math.ceil(articles.length / colCount) )];

  // chunk the articles into the array of rows
  const articleRows = rows.map( (row, idx) => articles.slice(idx * colCount, idx * colCount + colCount) );

  // map the rows as div.row
  const articleContent = articleRows.map((row, idx) => (
      <div className="row  justify-content-around  mt-4 mb-4" key={idx}>
        <CardGroup>
        { row.map( article =>
          <div className="col-sm-4">
            <Card
              className="h-100"
              key={article}>
              <Card.Body>
                <Card.Title className="text-left">{article.title}</Card.Title>
                <Card.Text className="text-left">
                  {article.type &&
                   <p className="mt-2"><strong className="text-left pull-left">{article.type.name}</strong></p>
                  }
                  <p>{article.summary}</p>
                </Card.Text>
              </Card.Body>
              <Card.Footer>
                <Card.Link
                  className="text-left"
                  target="_blank"
                  rel="nofollow"
                  href={article.url}
                  title="Read full article"
                  >
                  Read full article
                </Card.Link>
                <p className="text-left mt-4">
                 {article.authors ?
                   <strong>Author(s): {article.authors.map((author,idx) => <span className="csv">{author.name}</span>)}</strong>
                   :
                   <>
                    &nbsp;
                  </>
                 }
                 </p>
              </Card.Footer>
            </Card>
          </div>
        )}
        </CardGroup>
      </div> )
    );



  return (
    <>
      <div className="row mt-2 mb-4">
        <div className="col-6">
          <label htmlFor="authors">Authors</label>
          <Select
            className="w-100"
            name="authors"
            getOptionLabel={option => option.name}
            getOptionValue={option => option.id}
            onChange={selectedOption =>
              {
                setPage(1)
                setSelectedAuthor(selectedOption.id)
              }
            }
            options={authors}
          />
        </div>
        <div className="col-6">
          <label htmlFor="authors">Types</label>
          <Select
            className="w-100"
            name="types"
            getOptionLabel={option => option.name}
            getOptionValue={option => option.name.toLowerCase()}
            onChange={selectedOption =>
              {
                setPage(1)
                setSelectedType(selectedOption.name.toLowerCase())
              }
            }
            options={types}
          />
        </div>
      </div>
      <div className="row mt-2 mb-2">
        <div className="col">
          <h2  className="text-left">Number of articles: {articleCount}</h2>
        </div>
      </div>
      { !loading ?
        <>
          <ArticlePagination onPageSizeChange={pageSizeEventHandler} onItemClick={paginationEventHandler} page={page} pages_total={pagesTotal} page_size={pageSize}/>
          {articleContent}
          <ArticlePagination onPageSizeChange={pageSizeEventHandler} onItemClick={paginationEventHandler} page={page} pages_total={pagesTotal} page_size={pageSize}/>
        </>
      :
       <span>
       Loading...
       </span>
      }

    </>
  )
}

export default BFIArticlesList;
