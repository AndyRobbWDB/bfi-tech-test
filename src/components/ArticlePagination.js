import React, { useEffect , useState} from "react";
import Pagination from 'react-bootstrap/Pagination';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';

function ArticlePagination(props)  {

  const [items, setItems] = useState([]);

  useEffect(() => {
      // calculate total pages
    //const totalPages = Math.ceil(props.pages_total / pageSize);
    //debugger;
    let startPage, endPage;
    if (props.pages_total <= 10) {
        // less than 10 total pages so show all
        startPage = 1;
        endPage = props.pages_total;
    } else {
        // more than 10 total pages so calculate start and end pages
        if (props.page <= 6) {
            startPage = 1;
            endPage = 10;
        } else if (props.page + 4 >= props.pages_total) {
            startPage = props.pages_total - 9;
            endPage = props.pages_total;
        } else {
            startPage = props.page - 5;
            endPage = props.page + 4;
        }
    }

    // create an array of pages to ng-repeat in the pager control
    let pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i);

    let items = [];

    pages.forEach((number, i) => {
      items.push(
        <Pagination.Item
          onClick={e => {
            if (props.onItemClick)
              props.onItemClick(e.target.text);
          }}
          key={number}
          className={number === props.page ? '' : 'visible-desktop'}
          active={number === props.page}>
          {number}
        </Pagination.Item>,
      );
    });

    setItems(items);

  }, [props]);

  return (
      <div
        className="flex-desktop d-print-none"
      >
      <DropdownButton
        className="mr-3"
         variant="primary"
         title={'No per page' + ' ' + props.page_size}
         onSelect={e => {
           if (props.onPageSizeChange)
             props.onPageSizeChange(e);
         }}
       >
         <Dropdown.Item eventKey="3" active={"3" === props.page_size}>3</Dropdown.Item>
         <Dropdown.Item eventKey="9" active={"9" === props.page_size}>9</Dropdown.Item>
         <Dropdown.Item eventKey="18" active={"18" === props.page_size}>18</Dropdown.Item>
         <Dropdown.Item eventKey="45" active={"45" === props.page_size}>45</Dropdown.Item>
         <Dropdown.Item eventKey="90" active={"90" === props.page_size}>90</Dropdown.Item>
         <Dropdown.Item eventKey="180" active={"180" === props.page_size}>180</Dropdown.Item>

       </DropdownButton>
        <Pagination>


          <Pagination.First
            disabled={props.page === 1}
            onClick={e => {
              if (props.onItemClick)
                props.onItemClick(1);
            }}
          />
          <Pagination.Prev
            disabled={props.page === 1}
            onClick={e => {
              if (props.onItemClick)
                props.onItemClick(props.page-1);
            }}
          />
          <Pagination.Item
            disabled={props.page === 1}
            onClick={e => {
              if (props.onItemClick)
                props.onItemClick(1);
            }}>{1}
          </Pagination.Item>
          <Pagination.Ellipsis />
          {items}
          <Pagination.Ellipsis />
          <Pagination.Item
            disabled={props.page === props.pages_total}
            onClick={e => {
              if (props.onItemClick)
                props.onItemClick(props.pages_total);
            }}
          >{props.pages_total}</Pagination.Item>
          <Pagination.Next
            disabled={props.page === props.pages_total}
            onClick={e => {
              if (props.onItemClick)
                props.onItemClick(props.page+1);
            }}
          />
          <Pagination.Last
            disabled={props.page === props.pages_total}
            onClick={e => {
              if (props.onItemClick)
                props.onItemClick(props.pages_total);
            }}
           />
      </Pagination>
    </div>
  )

};


export default ArticlePagination
