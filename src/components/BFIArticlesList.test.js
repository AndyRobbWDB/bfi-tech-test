import React from "react";
import { render, screen } from "@testing-library/react";
import BFIArticlesList from "./BFIArticlesList";

import { apiGet } from '../api/APIService'

jest.mock("../api/APIService");

test("Loading articles data", () => {
  render(<BFIArticlesList />);
  expect(screen.getByText("Loading...")).toBeInTheDocument();
  expect(apiGet).toHaveBeenCalledTimes(3);

});
