import logo from './logo.svg';
import './App.css';
import BFIArticlesList from './components/BFIArticlesList'

function App() {
  return (
    <div className="App">
        <div className="container mt-4 ">
          <div className="row">
            <div className="col">
              <h1 className="text-left">BFI Tech Test</h1>
            </div>
          </div>

          <BFIArticlesList/>
        </div>
      </div>
  );
}

export default App;
