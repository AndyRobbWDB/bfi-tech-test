


export function  apiGet(url) {

  const requestOptions = {
      method: 'GET',

  };

  return fetch(url, requestOptions)
    .then(res =>
    {
      if(res.status!==200 && res.status!==201 && res.status!==202)
       {
          throw new Error(res.status)
       } else {
         return res.json()
       }
      }
    )
    .then(result => {
        return Promise.resolve(result);
    })
    ;
}
